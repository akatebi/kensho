import * as React from "react";
import PropTypes from "prop-types";
import cx from "classnames";
import { HTMLTable } from "@blueprintjs/core";

import HeaderCell from "./HeaderCell";

const debug = require("debug")("app:ContactsTable");

export default class ContactsTable extends React.Component {
  static propTypes = {
    columns: PropTypes.arrayOf(
      PropTypes.shape({
        name: PropTypes.string,
        key: PropTypes.string,
        type: PropTypes.oneOf(["category", "number", "string"])
      })
    ),
    contacts: PropTypes.arrayOf(PropTypes.object)
  };

  state = {
    filter: { name: "", phone: "", city: "", age: "" },
    sortPolarity: [+1, +1, +1, +1],
    sortIndex: 0
  };

  handleFilter = (key, event) => {
    const { value } = event.target;
    let { filter } = this.state;
    filter[key] = value;
    debug("filter", filter);
    this.setState({ filter });
  };

  sortBy = ({ key: sortKey }, idx) => {
    // debug("sortByKey", sortKey, idx);
    const sortPolarity = this.state.sortPolarity;
    sortPolarity.splice(idx, 1, sortPolarity[idx] * -1);
    const sortIndex = idx;
    this.setState({ sortKey, sortPolarity, sortIndex }, () =>
      debug("sortKey, sortPolarity", sortKey, sortPolarity, sortIndex)
    );
  };

  render() {
    const { filter } = this.state;
    let { columns, contacts } = this.props;
    // debug("#### columns", columns);
    // debug("#### contacts", contacts.slice(0, 4));
    const sortKey = this.state.sortKey;
    const sortPolarity = this.state.sortPolarity[this.state.sortIndex];
    contacts = contacts.sort((a, b) =>
      sortPolarity > 0 ? a[sortKey] < b[sortKey] : a[sortKey] > b[sortKey]
    );
    const filteredContacts = contacts
      .filter(contact =>
        contact.name.toUpperCase().includes(filter.name.toUpperCase())
      )
      .filter(contact => contact.phone.includes(filter.phone))
      .filter(contact =>
        contact.city.toUpperCase().includes(filter.city.toUpperCase())
      )
      .filter(contact =>
        new RegExp(`^${filter.age}`).test(new String(contact.age))
      );
    return (
      <HTMLTable striped interactive>
        <thead>
          <tr>
            {columns.map((col, idx) => (
              <HeaderCell
                onClick={this.sortBy.bind(this, col, idx)}
                filter={filter[col.key]}
                onFilter={this.handleFilter.bind(this, col.key)}
                {...col}
              />
            ))}
          </tr>
        </thead>
        <tbody>
          {filteredContacts.map(contact => (
            <tr key={contact.phone}>
              {columns.map(column => (
                <td key={column.key}>{contact[column.key]}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </HTMLTable>
    );
  }
}
