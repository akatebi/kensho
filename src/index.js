import * as React from "react";
import { render } from "react-dom";

import ContactsTable from "./ContactsTable";
import "./styles.css";

const root = document.createElement("div");
document.body.appendChild(root);

localStorage.setItem("debug", "app:*");

render(<ContactsTable />, root);
